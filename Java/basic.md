---
title: Java Basis
description: Java基础速查表
published: true
date: 2020-07-26T06:55:58.968Z
tags: 
editor: markdown
---

# 访问修饰符


| Modifier    | Class | Package | Subclass<br>(same Package) | Subclass<br/>(diff Package) | World<br>(same Package) |
| ----------- | ----- | ------- | -------------------------- | --------------------------- | ----------------------- |
| public      | +     | +       | +                          | +                           | +                       |
| protected   | +     | +       | +                          | +                           |                         |
| no modifier | +     | +       | +                          |                             |                         |
| private     | +     |         |                            |                             |                         |

- `+` : accessible	;	`blank` : not accessible;

规则:
1. private：只能在同一个类中访问
2. 没有修饰符：只能在同一包内访问
3. protected：同一包内所有类，其他包中子类
4. public: 所有同一个module的类

规则：
1. `Principle of Least Knowledge`: 只允许所需的最小可见度

注意：
1. 表格告诉我们允许访问与否，但什么是构成访问？访问实际上以复杂的方式与内部类和继承进行着交互
2. 类方法、内部类、静态方法是如何进行访问控制的？

## 示例

### 易于误解的protected

``` java
package fatherpackage;

public class Father
{
	protected void foo(){}
}

// ------------------------------------------

package sonpackage;

public class Son extends Father
{

}
```

`foo()`可以在3个上下文访问：

1. 和`foo()`定义在同一个包中的类，可以访问`foo()`

   ``` java
   package fatherpackage;
   
   public class SomeClass
   {
       public void someMethod(Father f, Son s)
       {
           f.foo();
           s.foo();
       }
   }
   
   // ------------------------------------------
   
   package fatherpackage;
   
   public class Son extends Father
   {
       public void sonMethod(Father f)
       {
           f.foo();
       }
   }
   ```

2. 在子类内部，可以通过 this 和 super 访问

   ``` java
   package sonpackage;
   
   public class Son extends Father
   {
       public void sonMethod()
       {
           this.foo();
           super.foo();
       }
   }
   ```

3. 在类型相同的引用上，注意这和this和super访问不同（重点在类型相同？）

   ``` java
   package fatherpackage;
   
   public class Father
   {
       public void fatherMethod(Father f)
       {
           f.foo(); // valid even if foo() is private
       }
   }
   
   // ------------------------------------------
   
   package sonpackage;
   
   public class Son extends Father
   {
       public void sonMethod(Son s)
       {
           s.foo();
       }
   }
   ```

在以下情况下不能访问：

1. 类型为父类，但是和`foo()`定义不在一个包中

   ``` java
   package sonpackage;
   
   public class Son extends Father
   {
       public void sonMethod(Father f)
       {
           f.foo(); // compilation error
       }
   }
   ```

2. 子类的同一个包内，但是不是子类（子类从其父类继承受保护的成员，并使它们对非子类私有）

   ``` java
   package sonpackage;
   
   public class SomeClass
   {
       public void someMethod(Son s) throws Exception
       {
           s.foo(); // compilation error
       }
   }
   ```

注意区分上面这5种情况中的以下三种：

``` java
package fatherpackage;

public class Son extends Father
{
    public void sonMethod(Father f)
    {
        f.foo(); // ok
    }
}

// ------------------------------------------

package sonpackage;

public class Son extends Father
{
    public void sonMethod(Son s)
    {
        s.foo(); // ok
    }
}

// ------------------------------------------

package sonpackage;

public class SomeClass
{
    public void someMethod(Son s) throws Exception
    {
        s.foo(); // compilation error
    }
}
```

# 参考
1. [What is the difference between public, protected, package-private and private in Java?](https://stackoverflow.com/questions/215497/what-is-the-difference-between-public-protected-package-private-and-private-in)
