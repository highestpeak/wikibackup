---
title: Interface
description: Interface 的一些写法
published: true
date: 2020-07-26T03:48:17.861Z
tags: 
editor: markdown
---

# Default
- 接口方法默认实现
- JAVA8+
``` java
public interface SampleInterface{
		Object sampleNormalMethoud();
    default void sampleDefaultMethoud(){
    		System.out.println("sample Default Methoud");
    }
}
```