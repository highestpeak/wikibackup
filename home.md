---
title: Home
description: 首页
published: true
date: 2020-07-24T06:07:28.457Z
tags: 
editor: markdown
---

# Home
这里是wiki首页
这个本地wiki站点主要是写一些步骤性的东西，或者偏向列表式的，他和文章不一样。
比如要整理linux的ls命令和要写ls的用途是不一样的，一个是字典式的工具书，一个是使用场景式的使用标准